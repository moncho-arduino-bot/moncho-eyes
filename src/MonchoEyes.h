/*
  MonchoEyes.h - Library that manages the eyes of Moncho
  Created by Jesus Merino, September 26, 2020.
  Released into the public domain.
*/

#ifndef MonchoEyes_h
#define MonchoEyes_h

typedef struct myframeType
{
    int animationIndex; // Index pointer into binaryArray signifying animation frame
    int frameDelay;     // Approx delay in MilliSeconds to hold display this animated frame
    int frameLuminance; // 0 ... 15, The intensity of the led matrix for a given frame
} frameType;

typedef struct
{
    int frameCountCurrent; // This field must be zero
    int frameDelayCurrent; // This field must be zero
    int movieLength;       // This field must be zero. Auto calculated at start up in 'loop()', requires 'EMARKER' to be last element in 'frameType' array
    int whichScreen;       // Which screen you wish to show this movie on.
    myframeType *movie;    // Pointer to movie of interest (ie. a collection of frames)
} frameAnimationDescriptor;

enum EyesPosition
{
    eyes_center,
    eyes_right,
    eyes_left,
    eyes_frown
};

class MonchoEyes
{
public:    
    MonchoEyes();
    void start();  
    void update();  
    void lookTo(EyesPosition newPosition);  
};

#endif
