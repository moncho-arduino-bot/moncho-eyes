#include <Arduino.h>
#include <MonchoEyes.h>

MonchoEyes moncho;
unsigned long time;
unsigned long startedTime;

void setup()
{
  moncho = MonchoEyes();
  Serial.begin(9600);
  moncho.start();

  startedTime = millis();
};

void loop() { 
  moncho.update();

  time = millis();
  if (time-startedTime == 5000) {
    moncho.lookTo(eyes_right);
  }

  if (time-startedTime == 10000) {
    moncho.lookTo(eyes_left);
  }

  if (time-startedTime == 20000) {
    moncho.lookTo(eyes_center);
  }
};

