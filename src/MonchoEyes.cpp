/*
  MonchoEyes.h - Library that manages the eyes of Moncho
  Created by Jesus Merino, September 26, 2020.
  Released into the public domain.
*/

// D3 = Digital O/P CS Pin
// D9 = Digital O/P Clock Pin
// D2 = Digital O/P Data Pin
#include <avr/pgmspace.h>
#include "LedControl.h"
#include "Arduino.h"
#include "EyesBytesDef.cpp"
#include <SimpleTimer.h>
#include "MonchoEyes.h"

#define DELAY_US 1000 // Delay time in MicroSeconds

#define SCREEN1 0

frameType lookRightMovie[5] = {
    {LeftEye9, 600, 1},
    {LeftEye10, 50, 1},
    {LeftEye12, 50, 1},
    {LeftEye13, 1000, 1},
    EMARKER};

frameType lookLeftMovie[5] = {
    {LeftEye9, 600, 1},
    {LeftEye26, 50, 1},
    {LeftEye27, 50, 1},
    {LeftEye28, 50, 1},
    EMARKER};

frameType frownMovie[5] = {
    {LeftEye5, 50, 3},
    {LeftEye6, 50, 4},
    {LeftEye7, 50, 5},
    {LeftEye8, 2000, 11},
    EMARKER};

frameType blinkCenterMovie[8] = {
    {LeftEye1, 3000, 1},
    {LeftEye2, 50, 1},
    {LeftEye3, 50, 1},
    {LeftEye4, 50, 1},
    {LeftEye3, 50, 1},
    {LeftEye2, 50, 1},
    {LeftEye1, 3000, 1},
    EMARKER};

frameType lookCenterFromRightMovie[5] = {
    {LeftEye13, 50, 1},
    {LeftEye12, 50, 1},
    {LeftEye11, 50, 1},
    {LeftEye9, 50, 1},
    EMARKER};

frameType lookRightFromLeftMovie[9] = {
    {LeftEye28, 50, 1},
    {LeftEye27, 50, 1},
    {LeftEye26, 50, 1},
    {LeftEye9, 50, 1},
    {LeftEye10, 50, 1},
    {LeftEye11, 50, 1},
    {LeftEye12, 50, 1},
    {LeftEye13, 50, 1},
    EMARKER};

frameType lookLeftFromRightMovie[9] = {
    {LeftEye13, 50, 1},
    {LeftEye12, 50, 1},
    {LeftEye11, 50, 1},
    {LeftEye10, 50, 1},
    {LeftEye9, 50, 1},
    {LeftEye26, 50, 1},
    {LeftEye27, 50, 1},
    {LeftEye28, 50, 1},
    EMARKER};

frameType blinkRightMovie[9] = {
    {LeftEye13, 100, 1},
    {LeftEye14, 50, 1},
    {LeftEye15, 50, 1},
    {LeftEye16, 50, 1},    
    {LeftEye16, 50, 1},
    {LeftEye15, 50, 1},
    {LeftEye14, 50, 1},
    {LeftEye13, 1000, 1},
    EMARKER};

frameType blinkLeftMovie[9] = {
    {LeftEye28, 100, 1},
    {LeftEye29, 50, 1},
    {LeftEye30, 50, 1},
    {LeftEye16, 50, 1},    
    {LeftEye16, 50, 1},
    {LeftEye30, 50, 1},
    {LeftEye29, 50, 1},
    {LeftEye28, 1000, 1},
    EMARKER};

frameType lookCenterFromLeftMovie[5] = {
    {LeftEye28, 50, 1},
    {LeftEye27, 50, 1},
    {LeftEye26, 50, 1},
    {LeftEye9, 600, 1},
    EMARKER};

frameType keepFrownMovie[3] = {
    {LeftEye8, 50, 10},
    {LeftEye8, 50, 10},
    EMARKER};

const int dataPin = 2; //2;
const int clkPin = 9;  //14;
const int csPin = 3;   //3;
LedControl lc = LedControl(dataPin, clkPin, csPin, 2);

// MonchoEyes Status control
SimpleTimer lcdTimer;
int currentFrameOfMovie = 0;
myframeType *currentMovie;
unsigned long acumDelay = 0;
EyesPosition currentEyePosition = eyes_center;
EyesPosition nextEyePosition = eyes_center;

inline void handleFrameAnimation()
{
    Serial.println("handleFrameAnimation");
    for (int i = 0; i < 8; i++)
    {
        const byte leftEye = pgm_read_byte(&binaryArray[currentMovie[currentFrameOfMovie].animationIndex].array1[i]);
        const byte rightEye = pgm_read_byte(&binaryArray[currentMovie[currentFrameOfMovie].animationIndex + 1].array1[i]);
        lc.setRow(0, i, leftEye);
        lc.setRow(1, i, rightEye);
    }
    lc.setIntensity(0, currentMovie[currentFrameOfMovie].frameLuminance);
    lc.setIntensity(1, currentMovie[currentFrameOfMovie].frameLuminance);

    currentFrameOfMovie++;
};

inline myframeType *handleEyesMovement()
{
    myframeType *returnValue = blinkCenterMovie;

    if (currentEyePosition == eyes_center && nextEyePosition == eyes_center)
    {
        returnValue = blinkCenterMovie;
    }

    if (currentEyePosition == eyes_center && nextEyePosition == eyes_right)
    {
        returnValue = lookRightMovie;
    }

    if (currentEyePosition == eyes_center && nextEyePosition == eyes_left)
    {
        returnValue = lookLeftMovie;
    }

    if (currentEyePosition == eyes_center && nextEyePosition == eyes_frown)
    {
        returnValue = frownMovie;
    }

    // Right

    if (currentEyePosition == eyes_right && nextEyePosition == eyes_center)
    {
        returnValue = lookCenterFromRightMovie;
    }

    if (currentEyePosition == eyes_right && nextEyePosition == eyes_left)
    {
        returnValue = lookLeftFromRightMovie;
    }

    if (currentEyePosition == eyes_right && nextEyePosition == eyes_frown)
    {
        returnValue = frownMovie;
    }

    if (currentEyePosition == eyes_right && nextEyePosition == eyes_right)
    {
        returnValue = blinkRightMovie;
    }

    // Left

    if (currentEyePosition == eyes_left && nextEyePosition == eyes_center)
    {
        returnValue = lookCenterFromLeftMovie;
    }

    if (currentEyePosition == eyes_left && nextEyePosition == eyes_right)
    {
        returnValue = lookRightFromLeftMovie;
    }

    if (currentEyePosition == eyes_left && nextEyePosition == eyes_frown)
    {
        returnValue = frownMovie;
    }

    if (currentEyePosition == eyes_left && nextEyePosition == eyes_left)
    {
        returnValue = blinkLeftMovie;
    }

    // Frown

    if (currentEyePosition == eyes_frown && nextEyePosition == eyes_center)
    {
        returnValue = blinkCenterMovie;
    }

    if (currentEyePosition == eyes_frown && nextEyePosition == eyes_left)
    {
        returnValue = lookLeftMovie;
    }

    if (currentEyePosition == eyes_frown && nextEyePosition == eyes_frown)
    {
        returnValue = keepFrownMovie;
    }

    if (currentEyePosition == eyes_frown && nextEyePosition == eyes_right)
    {
        returnValue = lookRightMovie;
    }

    Serial.println(currentEyePosition);
    Serial.println(nextEyePosition);
    Serial.println("-------");
    currentEyePosition = nextEyePosition;

    return returnValue;
};

inline void readNextAnimation()
{
    Serial.println("renderNextAnimation");
    currentMovie = handleEyesMovement();
    currentFrameOfMovie = 0;    
    acumDelay = 500;
    int index = 0;    

    do  // calculate the number of frames in the movie and store is movieLength
    {                
        lcdTimer.setTimeout(acumDelay, handleFrameAnimation);
        acumDelay += currentMovie[index].frameDelay;
        index++;
    } while (currentMovie[index].animationIndex != MAX_INT);

    lcdTimer.setTimeout(acumDelay + 100, readNextAnimation);
};

MonchoEyes::MonchoEyes()
{
    for (int x = 0; x < 2; x++)
    {
        lc.shutdown(x, false); //The MAX72XX is in power-saving mode on startup
        lc.setIntensity(x, 3); // Set the brightness to default value
        lc.clearDisplay(x);    // and clear the display
    }
};

void MonchoEyes::start()
{
    readNextAnimation();
}

void MonchoEyes::update()
{
    lcdTimer.run();
}

void MonchoEyes::lookTo(EyesPosition newPosition)
{
    nextEyePosition = newPosition;
}
